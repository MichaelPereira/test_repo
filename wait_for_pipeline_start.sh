
wait_for_pipeline_start() {
    repo_id="$1"
    
    pipeline_data="$(curl -s --header "PRIVATE-TOKEN: ${GITLABCOM_TOKEN}" "https://gitlab.com/api/v4/projects/${repo_id}/pipelines?sha=${CI_COMMIT_SHA}")"
    until [[ $(echo "${pipeline_data}" | jq 'length') != "0" ]]; do
        sleep 5
        pipeline_data="$(curl -s --header "PRIVATE-TOKEN: ${GITLABCOM_TOKEN}" "https://gitlab.com/api/v4/projects/${repo_id}/pipelines?sha=${CI_COMMIT_SHA}")"
    done
}

get_pipeline_id() {
    repo_id="$1"
    
    pipeline_data="$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${repo_id}/pipelines?sha=${CI_COMMIT_SHA}")"
    echo "${pipeline_data}" | jq '.[0].id'
}
set -x
project_path='5776899'

echo 'waiting for pipeline to start'
wait_for_pipeline_start "${project_path}"

echo 'retrieving pipeline id'

pipeline_id=$(get_pipeline_id "${project_path}")
echo "pipeline id: ${pipeline_id}"
echo "https://gitlab.com/api/v4/projects/${project_path}/pipelines/${pipeline_id}"
 
