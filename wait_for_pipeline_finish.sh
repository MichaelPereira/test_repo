get_pipeline_id() {
    repo_id="$1"
    
    pipeline_data="$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${repo_id}/pipelines?sha=${CI_COMMIT_SHA}")"
    echo "${pipeline_data}" | jq '.[0].id'
}
set -x
project_path='5776899'
pipeline_id=$(get_pipeline_id "${project_path}")
status="$(curl -s --header "PRIVATE-TOKEN: ${GITLABCOM_TOKEN}" "https://gitlab.com/api/v4/projects/${project_path}/pipelines/${pipeline_id}?sha=${CI_COMMIT_SHA}" | jq -r '.status')"

until [[ "${status}" == "success" ]] || [[ "${status}" == "failed" ]]; do
    status="$(curl -s --header "PRIVATE-TOKEN: ${GITLABCOM_TOKEN}" "https://gitlab.com/api/v4/projects/${project_path}/pipelines/${pipeline_id}?sha=${CI_COMMIT_SHA}" | jq -r '.status')"
    echo "pipeline status: ${status}"
    sleep 5
done
